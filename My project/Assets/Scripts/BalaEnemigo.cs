using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaEnemigo : MonoBehaviour
{
    public int dañoEnemigo = 10;
    public float tiempoDeVida = 6.0f;
    public float velocidadBala = 20.0f;

    void Start()
    {
        GetComponent<Rigidbody>().velocity = transform.forward * velocidadBala;
        Invoke("DestruirBala", tiempoDeVida);
    }

    void DestruirBala()
    {
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            other.GetComponent<VidaJugador>().RecibirDaño(dañoEnemigo);
            Destroy(gameObject);
        }
        else if (other.CompareTag("Pared"))
        {
            
            Destroy(gameObject);
        }
        else if (other.CompareTag("Escudo"))
        {
            other.GetComponent<VidaObjeto>().RecibirDaño(dañoEnemigo);
            Destroy(gameObject);
        }
    }  
    
}
