using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoBomba : MonoBehaviour
{
    public GameObject bombaPrefab;
    public float fuerzaLanzamiento = 10f;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            LanzarBomba();
        }
    }

    void LanzarBomba()
    {
        GameObject bomba = Instantiate(bombaPrefab, transform.position, transform.rotation);
        Rigidbody bombaRigidbody = bomba.GetComponent<Rigidbody>();

        if (bombaRigidbody != null)
        {
            bombaRigidbody.AddForce(transform.forward * fuerzaLanzamiento, ForceMode.Impulse);
        }
    }
}
