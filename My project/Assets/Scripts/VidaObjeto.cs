using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidaObjeto: MonoBehaviour
{
    private GameObject jugador;
    private GameManager gameManager;
    public int hp = 100;

    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("BalaEnemigo"))
        {
            RecibirDa�o(100); 
        }
    }
    public void RecibirDa�o(int da�o)
    {
        hp -= da�o;
        Debug.Log("Salud actual: " + hp);

        if (hp <= 0)
        {
            Destroy(gameObject);
            gameManager.ParedDestruida(gameObject);
            
            
        }
    }

    
}


