using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoEnemigo : MonoBehaviour
{
    public GameObject balaPrefab;
    public float intervaloDisparo = 2.0f; 
    private float tiempoUltimoDisparo = 0.0f;
    public float velocidadBala = 10.0f;

    void Update()
    {
        if (Time.time - tiempoUltimoDisparo > intervaloDisparo)
        {
            Debug.Log("Enemigo disparando.");
            Disparar();
            tiempoUltimoDisparo = Time.time;
        }
       
        
            GameObject jugador = GameObject.FindGameObjectWithTag("Jugador");
            transform.LookAt(jugador.transform);
           

        
    }

    void Disparar()
    {
        GameObject bala = Instantiate(balaPrefab, transform.position, transform.rotation);
        bala.GetComponent<Rigidbody>().velocity = transform.forward * velocidadBala;
    }
}
