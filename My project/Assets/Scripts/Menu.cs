using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public GestorDeAudio gestorDeAudio;
    void Start()
    {
       
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        if (gestorDeAudio != null)
        {
            gestorDeAudio.DetenerTodosLosSonidos();
        }
    }

    public void Jugar()
    {
        SceneManager.LoadScene("Level1");


    }

    public void Salir()
    {

        Application.Quit();

    }
}
