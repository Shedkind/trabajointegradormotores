using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactuable : MonoBehaviour
{
    private bool sostenido = false;

    public void Agarrar(Transform jugadorTransform, Transform puntoDeSostener)
    {
        sostenido = true;
        transform.parent = jugadorTransform;
       
        transform.localPosition = puntoDeSostener.localPosition;
        transform.localRotation = puntoDeSostener.localRotation;
      
        
    }

    public void Soltar()
    {
        sostenido = false;
        transform.parent = null; 
        
       
    }
}
