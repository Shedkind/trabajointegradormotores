using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataforma : MonoBehaviour
{

    public float velocidad = 2f;
    public float distanciaSubida = 5f;

    private bool subiendo = false;
    private Vector3 posicionInicial;

    void Start()
    {
        posicionInicial = transform.position;
    }

    void Update()
    {
        if (subiendo)
        {
            SubirPlataforma();
        }
        else
        {
            BajarPlataforma();
        }
    }

    void SubirPlataforma()
    {
        transform.Translate(Vector3.up * velocidad * Time.deltaTime);

        if (transform.position.y >= posicionInicial.y + distanciaSubida)
        {
            subiendo = false;
        }
    }

    void BajarPlataforma()
    {
        transform.Translate(Vector3.down * velocidad * Time.deltaTime);

        if (transform.position.y <= posicionInicial.y)
        {
            subiendo = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            subiendo = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            subiendo = false;
        }
    }
}
