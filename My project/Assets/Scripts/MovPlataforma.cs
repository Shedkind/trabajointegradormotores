using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovPlataforma : MonoBehaviour
{
    public float distanciaMovimientoZ = 5f;
    public float velocidad = 2f;

    private float posicionInicialZ;
    private float posicionFinalZ;
    private Vector3 escalaOriginalJugador;


    void Start()
    {
        posicionInicialZ = transform.position.z;
        posicionFinalZ = posicionInicialZ + distanciaMovimientoZ;
    }

    void Update()
    {
        float movimientoZ = Mathf.PingPong(Time.time * velocidad, distanciaMovimientoZ);
        transform.position = new Vector3(transform.position.x, transform.position.y, posicionInicialZ + movimientoZ);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            escalaOriginalJugador = other.transform.localScale;

            other.transform.parent = transform;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            other.transform.parent = null;

            other.transform.localScale = escalaOriginalJugador;
        }
    }
}

    
