using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pausa : MonoBehaviour
{
    public GameObject menuPausa;
    

    void Start()
    {
        DesactivarMenuPausa();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (EstaPausado())
            {
                ReanudarJuego();
            }
            else
            {
                PausarJuego();
            }
        }
    }

    void PausarJuego()
    {
        Time.timeScale = 0f; 
        menuPausa.SetActive(true);
        GestorDeAudio.instancia.DetenerTodosLosSonidos();
    }

    void ReanudarJuego()
    {
        Time.timeScale = 1f; 
        DesactivarMenuPausa();
        GestorDeAudio.instancia.ReproducirSonido("Cancion");
        Cursor.visible = false;

    }

    bool EstaPausado()
    {
        return Time.timeScale == 0f;
    }

    void DesactivarMenuPausa()
    {
        menuPausa.SetActive(false);
    }

    public void Reanudar()
    {
        Time.timeScale = 1f;
        DesactivarMenuPausa();
        GestorDeAudio.instancia.ReproducirSonido("Cancion");

    }
    public void Salir()
    {
    
        Application.Quit();
    }


}
