using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovCamara : MonoBehaviour
{
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        transform.Rotate(Vector3.up * mouseX * Sensibilidad.sensibilidadRaton);

        float nuevaRotacionX = transform.eulerAngles.x - mouseY * Sensibilidad.sensibilidadRaton;
        transform.eulerAngles = new Vector3(nuevaRotacionX, transform.eulerAngles.y, 0f);
    }
    void OnEnable()
    {
        
        enabled = true;
    }
}
