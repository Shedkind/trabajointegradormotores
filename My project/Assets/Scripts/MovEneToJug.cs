using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovEneToJug : MonoBehaviour
{
    private GameObject jugador;
    public int rapidez;
    public int da�o;

    private void Update()
    {
        GameObject jugador = GameObject.FindGameObjectWithTag("Jugador");
         transform.LookAt(jugador.transform);
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
        
     }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Jugador"))
        {
            
            other.GetComponent<VidaJugador>().RecibirDa�o(da�o);
            Destroy(gameObject);
        }
    }



}



