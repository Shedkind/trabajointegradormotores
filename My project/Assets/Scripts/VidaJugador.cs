using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class VidaJugador : MonoBehaviour
{
    public int salud = 100;
    public int vidas = 3;
    public int nivel = 1;
    private int saludActual;
    private bool juegoPausado = false;
    public int experiencia = 0;
    public int experienciaParaSiguienteNivel = 100;
    public TextMeshProUGUI textoVida;
    public TextMeshProUGUI txtNivel;
    public TextMeshProUGUI txtExperiencia;
    public TextMeshProUGUI textoVidas;
    public Slider barraExperiencia;
    private GameManager gameManager;
    public ContDisparo contDisparo;
    public List<Image> corazones;
    void Awake()
    {
        gameManager = GameObject.FindObjectOfType<GameManager>();

        if (gameManager == null)
        {
            Debug.LogError("GameManager no se ha inicializado correctamente en VidaJugador.");
        }
    }

    private void Start()
    {

        saludActual = salud;
        ActualizarTextoVida();
 
        ActualizarExp();

        // Encuentra la referencia al GameManager
        gameManager = GameObject.FindObjectOfType<GameManager>();

        if (gameManager == null)
        {
            Debug.LogError("No se encontr� el objeto GameManager en la escena.");
        }

        contDisparo = GetComponent<ContDisparo>();
    }

    
    void ActualizarTextoVida()
    {
        if (textoVida != null)
        {
            textoVida.text = "SALUD: " + saludActual.ToString();
        }
        if (textoVidas != null)
        {
            textoVidas.text = "VIDAS: " + vidas.ToString();  
        }
    }
    void ActualizarCorazones()
    {
        for (int i = 0; i < corazones.Count; i++)
        {
            corazones[i].enabled = i < vidas;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (!juegoPausado)
        {
            if (other.CompareTag("RecuperarSalud"))
            {
                RecuperarSalud(20);
                Destroy(other.gameObject);
            }
            else if (other.CompareTag("Ganador"))
            {
                if (!HayEnemigosActivos())
                {
                    Debug.Log("�Has ganado!");
                    if (gameManager != null)
                    {
                        gameManager.GanarJuego();
                    }
                    else
                    {
                        Debug.LogError("GameManager es nulo en VidaJugador.");
                    }
                }
            }
        }
    }

    public void GanarExperiencia(int cantidad)
    {
        experiencia += cantidad;
        Debug.Log("�Has ganado " + cantidad + " puntos de experiencia!");
        while (experiencia >= experienciaParaSiguienteNivel)
        {
            SubirNivel();
        }
        ActualizarExp();
    }

    private void SubirNivel()
    {
        nivel++;
        experiencia -= experienciaParaSiguienteNivel;
        experienciaParaSiguienteNivel = CalcularExperienciaParaSiguienteNivel();
        ActualizarExp();


        Debug.Log("�Subiste de nivel! Nivel actual: " + nivel);
        contDisparo.HabilitarDisparoMultiple();


    }
    private void ActualizarExp()
    {
        
        txtNivel.text =  nivel.ToString();
        txtExperiencia.text = "EXP: " + experiencia.ToString();
        barraExperiencia.value = (float)experiencia / experienciaParaSiguienteNivel;
    }

    private int CalcularExperienciaParaSiguienteNivel()
    {
        
        return nivel * 100; 
    }

    public void RecuperarSalud(int cantidad)
    {
        salud += cantidad;
        saludActual = salud;
        ActualizarTextoVida();
        Debug.Log("El jugador se est� curando. Salud actual: " + saludActual);
    }

    bool HayEnemigosActivos()
    {
        GameObject[] enemigos = GameObject.FindGameObjectsWithTag("Enemigo");
            
        foreach (GameObject enemigo in enemigos)
        {
            if (enemigo.activeInHierarchy)
            {
                return true;
            }
        }

        return false;
    }
    public void RecibirDa�o(int da�o)
    {
        salud -= da�o;

        if (salud <= 0)
        {
            vidas--;

            if (vidas <= 0)
            {
                if (!juegoPausado)
                {
                    juegoPausado = true;
                    gameManager.PerderJuego();
                    Debug.Log("Juego pausado por Game Over");
                }
            }
            else
            {
                saludActual = salud;
                Respawn();
                ActualizarTextoVida();
                ActualizarCorazones();
            }
        }
        else
        {
            saludActual = salud;
            ActualizarTextoVida();
        }
        ActualizarCorazones();
    }

    

    private void Respawn()
    {
        salud = 100; 
        saludActual = salud;
        ActualizarTextoVida();
        transform.position = new Vector3(2f, 2f, -9f);
    }

    public void Inicializar()
    {
        juegoPausado = false;
        Time.timeScale = 1;
    }
}
