using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;   

public class ContSensi : MonoBehaviour
{
    public Slider sliderSensibilidad;

    void Start()
    {
        sliderSensibilidad.value = Sensibilidad.sensibilidadRaton;
        sliderSensibilidad.onValueChanged.AddListener(CambiarSensibilidad);
    }

    void CambiarSensibilidad(float nuevaSensibilidad)
    {
        Sensibilidad.sensibilidadRaton = nuevaSensibilidad;
    }
}
