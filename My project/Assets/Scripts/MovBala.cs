using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovBala : MonoBehaviour
{
    public float tiempoDeVida = 3.0f;
    public float velocidadBala = 10.0f;
    public int da�o = 25;

    void Start()
    {
        GetComponent<Rigidbody>().velocity = transform.forward * velocidadBala;
        Invoke("DestruirBala", tiempoDeVida);
    }

    void DestruirBala()
    {
        Destroy(gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Bala entr� en colisi�n con algo.");

        if (other.CompareTag("Enemigo"))
        {
            EnemigoInvul enemigoInvulnerable = other.GetComponent<EnemigoInvul>();

            if (enemigoInvulnerable != null && enemigoInvulnerable.EstaInvulnerable())
            {
                // La bala no hace da�o al enemigo invulnerable.
                Destroy(gameObject);
            }
            else
            {
                // El enemigo no est� invulnerable, causa da�o.
                other.GetComponent<VidaEnemigo>().RecibirDa�o(da�o);
                Destroy(gameObject);
            }
        }
        else if (other.CompareTag("Pared"))
        {
            other.GetComponent<VidaObjeto>().RecibirDa�o(da�o);
            Destroy(gameObject);
        }
        else if (other.CompareTag("ParedR"))
        {
            other.GetComponent<ParedRompible>().RecibirDa�o(da�o);
            Destroy(gameObject);
        }
    }

    void RealizarDa�o(GameObject objetivo) 
    {


        VidaEnemigo vidaEnemigo = objetivo.GetComponent<VidaEnemigo>();
        if (vidaEnemigo != null)
        {
            vidaEnemigo.RecibirDa�o(da�o);
        }

    }


}

