using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContDisparo : MonoBehaviour
{
public GameObject balaPrefab;
public Transform puntoDeDisparo;
public float tiempoEntreDisparos = 0.5f;
private float tiempoUltimoDisparo;
private bool disparoMultipleActivado = false;
public float velocidadBala = 10.0f;

public VidaJugador jugador;

void Start()
{
    jugador = GetComponent<VidaJugador>();
}

void Update()
{
    if (Time.time - tiempoUltimoDisparo > tiempoEntreDisparos)
    {
        if (Input.GetButtonDown("Fire1"))
        {
            if (disparoMultipleActivado)
            {
                DispararMultiple();
                GestorDeAudio.instancia.ReproducirSonido("Disparo");
            }
            else
            {
                Disparar();
                GestorDeAudio.instancia.ReproducirSonido("Disparo");
            }
        }
    }
}

void Disparar()
{
    Instantiate(balaPrefab, puntoDeDisparo.position, puntoDeDisparo.rotation).GetComponent<Rigidbody>().velocity = transform.forward * velocidadBala;
    tiempoUltimoDisparo = Time.time;
}

void DispararMultiple()
{
    Disparar();
    Invoke("Disparar", 0.2f);

    tiempoUltimoDisparo = Time.time;
}

public void HabilitarDisparoMultiple()
{
    disparoMultipleActivado = true;
    Debug.Log("Disparo m�ltiple activado");
}

public void DeshabilitarDisparoMultiple()
{
    disparoMultipleActivado = false;
    Debug.Log("Disparo m�ltiple desactivado");
}
}
