using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaRelentizadora : MonoBehaviour
{
    public float velocidadRalentizacion = 0.5f; 
    public float duracionRalentizacion = 2.0f; 

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            MovJugador movJugador = other.GetComponent<MovJugador>();

            if (movJugador != null)
            {

                movJugador.Ralentizar(velocidadRalentizacion, duracionRalentizacion);

            }
          

            Destroy(gameObject);
        }
        else if (other.CompareTag("Pared"))
        {

            Destroy(gameObject);
        }
    }
}
