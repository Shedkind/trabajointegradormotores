using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{
    public int da�o = 100;

    private void OnTriggerEnter(Collider other)
    {
       
        if (other.CompareTag("Jugador"))
        {
            
            VidaJugador vidaJugador = other.GetComponent<VidaJugador>();
            if (vidaJugador != null)
            {
                vidaJugador.RecibirDa�o(da�o);
            }
        }
        else if (other.CompareTag("Enemigo"))
        {
            VidaEnemigo vidaEnemigo = other.GetComponent<VidaEnemigo>();
            if (vidaEnemigo != null)
            {
                vidaEnemigo.RecibirDa�o(da�o);
            }
        }
    }
}
