    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class EnemigoInvul : MonoBehaviour
    {
        public float tiempoInvulnerabilidad = 2.0f;
        public Color colorInvulnerable = Color.red; 
        private Color colorOriginal; 
        private bool enInvulnerabilidad = false;

        void Start()
        {
            colorOriginal = GetComponent<Renderer>().material.color;

            StartCoroutine(GestionarInvulnerabilidad());
        }

        IEnumerator GestionarInvulnerabilidad()
        {
            while (true)
            {
                yield return new WaitForSeconds(tiempoInvulnerabilidad);

                enInvulnerabilidad = true;

                GetComponent<Renderer>().material.color = colorInvulnerable;

                Debug.Log("El enemigo ahora es invulnerable y cambi� de color");

                yield return new WaitForSeconds(tiempoInvulnerabilidad);

                enInvulnerabilidad = false;

                GetComponent<Renderer>().material.color = colorOriginal;

                Debug.Log("El enemigo ya no es invulnerable y volvi� a su color original");
            }
        }
        void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Bala") && enInvulnerabilidad)
            {
                Destroy(other.gameObject); 
            }
       
        }
    public bool EstaInvulnerable()
    {
        return enInvulnerabilidad;
    }


}
