using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    
    public GameObject[] enemigos;
    public GameObject enemigoPrefab;
    public Transform[] puntosSpawnEnemigos;
    public Transform[] puntosSpawn;
    public Transform puntoSpawnJugador;
    private VidaJugador vidaJugador; 
    public TMP_Text textoPerdiste;
    public TMP_Text textoGanaste;
    public TMP_Text textoEnter;
    public TextMeshProUGUI textoTemporizador;  
    public TextMeshProUGUI textoEnemigosRestantes;
    public float tiempoLimite = 120f; 
    private float tiempoRestante;
    public   int enemigosRestantes;
    public ContDisparo scriptContDisparo;
    public Slider sliderTiempo;

    void Start()
    {
        tiempoRestante = tiempoLimite;
        IniciarJuego();
        ActualizarTextoTemporizador();
        enemigosRestantes = 8;  
        ActualizarTextoEnemigosRestantes();
        scriptContDisparo = FindObjectOfType<ContDisparo>();
    }

  

    void IniciarJuego()
    { 
        vidaJugador = FindObjectOfType<VidaJugador>();
        vidaJugador.Inicializar();
        RespawnJugador();
    }

    void SpawnEnemigos()
    {
        for (int i = 0; i < enemigos.Length; i++)
        {
            GameObject nuevoEnemigo = Instantiate(enemigoPrefab, puntosSpawnEnemigos[i].position, Quaternion.identity);
        }
    }

    public void PerderJuego()
    {
        Time.timeScale = 0;
        textoPerdiste.text = "�GAME OVER!";
        textoEnter.text = "Presiona Enter para volver al Menu";
    }

    public void GanarJuego()
    {
        Time.timeScale = 0;
        textoGanaste.text = "�GANASTE!";
        textoEnter.text = "Presiona Enter para volver al Menu";
    }

    public void ReiniciarJuego()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }
      
    
    public void RespawnJugador()
    {
       
        vidaJugador.transform.position = puntoSpawnJugador.position;
    }
    public void ParedDestruida(GameObject paredDestruida)
    {
        
        for (int i = 0; i < enemigos.Length; i++)
        {
            
            GameObject nuevoEnemigo = Instantiate(enemigoPrefab, puntosSpawnEnemigos[i].position, Quaternion.identity);
        }
    }

    void Update()
    {
        ActualizarTextoTemporizador();
        if (Input.GetKeyDown(KeyCode.Return))
        {
            VolverAlMenu();
            FindObjectOfType<GestorDeAudio>().DetenerTodosLosSonidos();
        }
      
        tiempoRestante -= Time.deltaTime;
        
        
        if (tiempoRestante <= 0)
        {
            PerderJuego(); 
        }
        else if (TodosEnemigosDestruidos() && tiempoRestante > 0)
        {
            GanarJuego(); 
        }

    }
    void ActualizarTextoTemporizador()
    {

        if (textoTemporizador != null && sliderTiempo != null)
        {
            textoTemporizador.text =  Math.Round(tiempoRestante, 0).ToString();
                float valorNormalizado = tiempoRestante / tiempoLimite;
            sliderTiempo.value = valorNormalizado;
        }
    }

    public void AgregarTiempo(float tiempo)
    {
        tiempoRestante += tiempo;
    }

    bool TodosEnemigosDestruidos()
    {
        
        foreach (GameObject enemigo in enemigos)
        {
            if (enemigo.activeInHierarchy)
            {
                return false; 
            }
        }
        return true; 
    }
    public void ActualizarTextoEnemigosRestantes()
    {
        // if (textoEnemigosRestantes != null)
        //{
        //   textoEnemigosRestantes.text = "ENEMIGOS RESTANTES: " + enemigosRestantes;
        //}
        GameObject[] enemigosActivos = GameObject.FindGameObjectsWithTag("Enemigo");
        enemigosRestantes = enemigosActivos.Length;

        if (textoEnemigosRestantes != null)
        {
            textoEnemigosRestantes.text = "ENEMIGOS RESTANTES: " + enemigosRestantes;
        }

    }



    void VolverAlMenu()
    {
        SceneManager.LoadScene("Menu"); 
    }
}

