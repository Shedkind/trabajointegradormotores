using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovJugador : MonoBehaviour
{

    public float alturaSalto = 5.0f;
    public int maximoSaltos = 2; 
    private int saltosRealizados = 0;
    public float rapidezDesplazamiento = 10.0f;
    public float tiempoEsperaEntreDashes = 5f;
    public float duracionDash = 0.5f;
    public float velocidadDash = 10f;
    private bool puedeDash = true;
    public Image cooldownImage;
    private float tiempoAcumulado = 0f;
    private bool enEscalera = false;
    private float velocidadEscalera = 5.0f;
    private GestorDeAudio gestorDeAudio;
    public float rangoInteraccion = 2f;
    private GameObject objetoSostenido;
    public Transform puntoDeSostener;   



    void Start()
        {
        gestorDeAudio = FindObjectOfType<GestorDeAudio>();
        if (gestorDeAudio == null)
        {
            Debug.LogError("GestorDeAudio no encontrado en la escena.");
        }

        Cursor.lockState = CursorLockMode.Locked;
        GestorDeAudio.instancia.ReproducirSonido("Cancion");

        }

    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime; 
        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (enEscalera)
        {
            float movimientoVertical = Input.GetAxis("Vertical") * velocidadEscalera;
            movimientoVertical *= Time.deltaTime;
            transform.Translate(0, movimientoVertical, 0);
        }

        if (Input.GetKeyDown("escape"))
            {
                Cursor.lockState = CursorLockMode.None;
            }

        if (Input.GetButtonDown("Jump")) 
            
        
        
          {
            Saltar();

          }
        if (Input.GetKeyDown(KeyCode.LeftShift) && puedeDash)
        {
            StartCoroutine(Dash());
        }

        UpdateCooldownImage();

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (objetoSostenido == null)
            {
                AgarrarObjeto();
            }
            else
            {
                SoltarObjeto();
            }
        }

    }

    void AgarrarObjeto()
    {
        Ray rayo = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(rayo, out hit, rangoInteraccion))
        {
            Interactuable interactuable = hit.collider.GetComponent<Interactuable>();

            if (interactuable != null)
            {
                objetoSostenido = interactuable.gameObject;
                interactuable.Agarrar(transform, puntoDeSostener);
            }
        }
    }

    void SoltarObjeto()
    {
        if (objetoSostenido != null)
        {
            Interactuable interactuable = objetoSostenido.GetComponent<Interactuable>();

            if (interactuable != null)
            {
                interactuable.Soltar();
                objetoSostenido = null;
            }
        }
    }

    void UpdateCooldownImage()
    {
        if (!puedeDash)
        {
            tiempoAcumulado += Time.deltaTime; 

            
            cooldownImage.fillAmount = Mathf.Clamp01(tiempoAcumulado / tiempoEsperaEntreDashes);

           
            if (tiempoAcumulado >= tiempoEsperaEntreDashes)
            {
                tiempoAcumulado = 0f; 
                cooldownImage.fillAmount = 1f; 
                puedeDash = true;
            }
        }
    }

    void Saltar()
    {
        if (saltosRealizados < maximoSaltos)
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, alturaSalto, 0);

            saltosRealizados++;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Suelo"))
        {
            saltosRealizados = 0;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Musica") && gestorDeAudio != null)
        {
            Debug.Log("Cambiando m�sica al atravesar la puerta.");

            gestorDeAudio.PausarSonido("Cancion");

            gestorDeAudio.ReproducirSonido("CancionEaster");
        }
        if (other.CompareTag("Escalera"))
        {
            enEscalera = true;
            Debug.Log("Entr� en la escalera");
            GetComponent<Rigidbody>().useGravity = false;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Musica") && gestorDeAudio != null)
        {
            Debug.Log("Volviendo a la m�sica original al salir de la puerta.");

            gestorDeAudio.ReanudarSonido("Cancion");

            gestorDeAudio.DetenerSonido("CancionEaster");
        }
        if (other.CompareTag("Escalera"))
        {
            enEscalera = false;
            Debug.Log("Sali� de la escalera");
            GetComponent<Rigidbody>().useGravity = true;
        }
    }


   
    public void Ralentizar(float factorRalentizacion, float duracion)
    {
        StartCoroutine(RalentizarCorrutina(factorRalentizacion, duracion));
    }

    private IEnumerator RalentizarCorrutina(float factorRalentizacion, float duracion)
    {
        rapidezDesplazamiento *= factorRalentizacion;

        yield return new WaitForSeconds(duracion);

        rapidezDesplazamiento /= factorRalentizacion;
    }

    IEnumerator Dash()
    {
        puedeDash = false;

        Vector3 posicionInicial = transform.position;
        Vector3 direccionDash = transform.forward;
        Vector3 posicionFinal = transform.position + direccionDash * velocidadDash;

        float tiempoPasado = 0f;
        while (tiempoPasado < duracionDash)
        {
            transform.position = Vector3.Lerp(posicionInicial, posicionFinal, tiempoPasado / duracionDash);
            tiempoPasado += Time.deltaTime;
            yield return null;
        }

        yield return new WaitForSeconds(tiempoEsperaEntreDashes);
        puedeDash = true;
    }



}
