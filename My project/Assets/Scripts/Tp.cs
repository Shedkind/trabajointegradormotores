using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tp : MonoBehaviour
{
    public Transform destino; 

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            TeletransportarJugador(other.transform);
        }
    }

    void TeletransportarJugador(Transform jugadorTransform)
    {
        jugadorTransform.position = destino.position;
        
    }
}
