    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class VidaEnemigo : MonoBehaviour
    { 
        private GameObject jugador;
        private GameManager gameManager;
        public int hp;
        private DisparoEnemigo disparoEnemigo;
        public int experienciaAlMatar = 50;
        private bool enInvulnerabilidad = false;

    void Start()
    {

        hp = 100;
        jugador = GameObject.Find("Jugador");
        disparoEnemigo = GetComponent<DisparoEnemigo>();
        gameManager = FindObjectOfType<GameManager>();
    }



    public void RecibirDa�o(int da�o)
    {
        hp -= da�o;
        Debug.Log("Salud actual: " + hp);

        if (hp <= 0)
        {
            gameObject.SetActive(false);
            Debug.Log("Enemigo destruido.");

            if (jugador != null)
            {
                jugador.GetComponent<VidaJugador>().GanarExperiencia(experienciaAlMatar);
            }



            if (gameManager != null)
            {
                gameManager.enemigosRestantes--;
                gameManager.ActualizarTextoEnemigosRestantes();
            }
        }
    }

}