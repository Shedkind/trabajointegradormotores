using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Corazones : MonoBehaviour
{
    public void ActualizarCorazon(bool activo)
    {
        gameObject.SetActive(activo);
    }
}
