using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomba : MonoBehaviour
{

    public float radioExplosion = 5f;
    public int da�o = 50;
    public float tiempoDeExplosion = 3f;
    public GameObject sparkPrefab;
    private static int bombasLanzadas = 0;



    void Start()
    {
        if (bombasLanzadas < 1)
        {
            bombasLanzadas++;
            Invoke("Explotar", tiempoDeExplosion);
        }
        else
        {
            Destroy(gameObject); 
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        CancelInvoke("Explotar");
        Explotar();
    }

    void Explotar()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radioExplosion);

        foreach (Collider collider in colliders)
        {
            if (collider.CompareTag("Enemigo"))
            {
                collider.GetComponent<VidaEnemigo>().RecibirDa�o(da�o);
            }
        }

       
        if (sparkPrefab != null)
        {
            GameObject spark = Instantiate(sparkPrefab, transform.position, Quaternion.identity);
            Destroy(spark, 2f); 
        }

        bombasLanzadas--;
        Destroy(gameObject);

    }
}
