using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject enemigoPrefab;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bala")
        {
            for (int i = 0; i < 3; i++)
            {
                Instantiate(enemigoPrefab, transform.position, Quaternion.identity);
            }

            Destroy(gameObject);
        }
    }
}
