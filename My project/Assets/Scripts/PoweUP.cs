using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoweUP : MonoBehaviour
{

    public float tiempoAgregar = 10f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            GameManager gameManager = FindObjectOfType<GameManager>();

            if (gameManager != null)
            {
                gameManager.AgregarTiempo(tiempoAgregar);
                Destroy(gameObject); 
            }
        }
    }


}
