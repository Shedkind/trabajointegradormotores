using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoweUpSalud : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            other.GetComponent<VidaJugador>().RecuperarSalud(20); 
            Destroy(gameObject);
        }
    }
}
